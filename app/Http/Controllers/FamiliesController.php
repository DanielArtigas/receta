<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Families;
use App\Ingredientes;
use App\Recetas;
use App\User;
use Auth;
use Session;
class FamiliesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except('index','show');
    }

    public function index()
    {
        $families= Families::all();
        $user = \Auth::user();
        if($user->can('manage',Families::class)){
            $families= Families::all();

        }else{
            $families = Families::where('user_id',$user->id)->get();
        }

        return view('families.index',['families'=>$families]);
    }

    public function create()
    {
        $families=Families::all();
        $this->authorize('view', $families);
        return view('families.create',['families'=>$families]);
    }

    public function store(Request $request)
    {
        $rules=[
            'name' => 'required|max:255'  ,
             'user_id' => 'exists:users,id' ,
          ];
 
 
          $messages=[
            'required'=>'El nombre debe estar requerido',
            'max'=>'maximo 255 caracteres'
 
          ];
 
         $request->validate($rules,$messages);
 
         $family = new Families;
         $family->fill($request->all());
         $family->user_id = \Auth::user()->id;
         $family->save();
 
 
 
       return redirect('/families');
    }

    public function show($id)
    {
        $families = Families::find($id);
        return view('families.show', ['families' => $families]);
    }

    public function edit($id)
    {
        $family=Families::find($id);
        $this->authorize('view', $family);
        return view('families.edit', ['family'=>$family]);
    }

    public function update(Request $request, $id)
    {
        $rules=[
            'name' => 'required|max:255'  ,
             'user_id' => 'exists:users,id' ,
            ];
    
            $messages=[
               'required'=>'El nombre debe estar requerido',
               'max'=>'maximo 255 caracteres'
    
            ];
    
            $request->validate($rules,$messages);
    
            $family = Families::find($id);
            $family->user_id = \Auth::user()->id;
            $family->fill($request->all());
            $family->save();  
    
          return redirect('/families');
    }

    public function destroy($id)
    {
        Families::destroy($id);
        return back();
    }
}
