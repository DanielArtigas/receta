<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Recetas;
use App\Families;
use Auth;
class RecetasController extends Controller
{

    public function __construct() {
        $this->middleware('auth')->except('index','show');
    }

    public function index()
    {
        $recetas = Recetas::all();
        return view('recetas.index',['recetas'=>$recetas]);
    }

    public function create()
    {
        $recetas = Recetas::all();
        $families = Families::all();
        return view('recetas.create',['recetas'=>$recetas,'families'=>$families]);
    }

    public function store(Request $request)
    {
        $rules=[
            'name' => 'required|max:255'  ,
            'user_id' => 'exists:users,id' ,
            'family_id' => 'exists:familias,id' ,
         ];

         $messages=[
           'required'=>'El nombre debe estar requerido',
           'max'=>'Máximo 255 caracteres',
           'exists'=>"Debe aparecer la familia_id"

         ];

        $request->validate($rules, $messages);

        $receta = new Recetas;
        $receta->fill($request->all());
        $receta->user_id = \Auth::user()->id;
        $receta->save();



      return redirect('/recetas');
    }

    public function show($id)
    {
        $receta= Recetas::find($id);
        return view('recetas.show',['receta'=>$receta]);
    }

    public function edit($id)
    {
        $receta= Recetas::find($id);
        $families = Families::all();
        return view('recetas.edit',['receta'=>$receta,'families'=>$families]);
    }

    public function update(Request $request, $id)
    {
        $rules=[
            'name' => 'required|max:255'  ,
            'user_id' => 'exists:users,id' ,
            'family_id' => 'exists:familias,id' ,
         ];

         $messages=[
           'required'=>'El nombre debe estar requerido',
           'max'=>'Máximo 255 caracteres',
           'exists'=>"Debe aparecer la familia_id"

         ];

        $request->validate($rules, $messages);

        $receta = Recetas::find($id);
        $receta->fill($request->all());
        $receta->user_id = \Auth::user()->id;
        $receta->save();



      return redirect('/recetas');
    }

    public function destroy($id)
    {
        Recetas::destroy($id);
        return back();
    }

    public function nombreReceta($id,Request $request){
        $receta=Recetas::find($id);
        $request->session()->put('receta',$receta);

        return back();
    }
}
