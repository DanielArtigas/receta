<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Ingredientes;
class IngredienteController extends Controller
{

    public function index()
    {
         return Ingredientes::with('user')->get();
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $rules=[
            'name' => 'required|max:255'  ,
            'user_id' => 'exists:users,id' ,
         ];

         $messages=[
           'required'=>'El nombre debe estar requerido',
           'max'=>'maximo 255 caracteres'

         ];

         $validator=Validator::make($request->all(),$rules,$messages);

         if($validator->fails()) {
            return response()->json($validator->errors(),400);
        }

        $ingrediente= new Ingredientes;
        $ingrediente->fill($request->all());
        $ingrediente->save();

        return $ingrediente;
    }

    public function show($id)
    {
        $ingrediente=Ingredientes::with('user')->find($id);
        if($ingrediente){
            return $ingrediente;
        }else{
            return response()->json([
                'message'=>"Ingrediente no encontrado",
            ],400);
        }
    }

    public function edit($id)
    {

    }

    public function update(Request $request, $id)
    {
        $rules=[
            'name' => 'required|max:255'  ,
            'user_id' => 'exists:users,id' ,
         ];

         $messages=[
           'required'=>'El nombre debe estar requerido',
           'max'=>'maximo 255 caracteres'

         ];

         $validator=Validator::make($request->all(),$rules,$messages);

         if($validator->fails()) {
            return response()->json($validator->errors(),400);
        }

        $ingrediente=Ingredientes::with('user')->find($id);

        if(!$ingrediente){
            return response()->json([
                 'message'=>"ingrediente no actualizado ni encontrado",
            ],404);
        }

        $ingrediente->fill($request->all());
        $ingrediente->save();
        $ingrediente->refresh();

        return $ingrediente;
    }

    public function destroy($id)
    {
        Ingredientes::destroy($id);
        return response()->json([
            'message'=>'Ingrediente destruido',
        ],201);
    }
}