<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Families;
use App\Recetas;
class FamiliaController extends Controller
{

    public function index()
    {
        return Families::with('user')->get();
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {

         $rules=[
           'name' => 'required|max:255'  ,
           'user_id' => 'exists:users,id' ,
         ];


         $messages=[
           'required'=>'El nombre debe estar requerido',
           'max'=>'maximo 255 caracteres'

         ];

         $validator=Validator::make($request->all(),$rules,$messages);

         if($validator->fails()) {
            return response()->json($validator->errors(),400);
        }

        $familia= new Families;
        $familia->fill($request->all());
        $familia->save();

        return $familia;
    }

    public function show($id)
    {
        $familia=Families::with('user')->find($id);
        if($familia){
            return $familia;
        }else{
            return response()->json([
                'message'=>"Lista no encontrada",
            ],400);
        }

    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        $rules=[
           'name' => 'required|max:255'  ,
           'user_id' => 'exists:users,id' ,
         ];


         $messages=[
           'required'=>'El nombre debe estar requerido',
           'max'=>'maximo 255 caracteres'

         ];

         $validator=Validator::make($request->all(),$rules,$messages);

         if($validator->fails()) {
            return response()->json($validator->errors(),400);
        }

        $familia=Familia::with('user')->find($id);

        if(!$familia){
            return response()->json([
                 'message'=>"familia no actualizada ni encontrada",
            ],404);
        }

        $familia->fill($request->all());
        $familia->save();
        $familia->refresh();

        return $familia;
    }

    public function destroy($id)
    {
        Families::destroy($id);
        return response()->json([
            'message'=>'Familia destruida',
        ],201);
    }
}