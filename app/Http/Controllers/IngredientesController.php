<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Families;
use App\Ingredientes;
use App\Recetas;
use App\User;
use Auth;

class IngredientesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except('index','show');
    }

    public function index()
    {
        $user = \Auth::user();
       if($user->can('manage',Ingredientes::class)) {
         $ingredientes=Ingredientes::all();

        } else {
        $ingredientes = Ingredientes::where('user_id',$user->id)->get();
        }

        return view('ingredientes.index',['ingredientes'=>$ingredientes]);
    }

    public function create()
    {
        $ingredientes=Ingredientes::all();
        $this->authorize('view', $ingredientes);
        return view('ingredientes.create',['ingredientes'=>$ingredientes]);
    }

    public function store(Request $request)
    {
        $rules=[
            'name' => 'required|max:255'  ,
            'user_id' => 'exists:users,id' ,
         ];

         $messages=[
           'required'=>'El nombre debe estar requerido',
           'max'=>'maximo 255 caracteres'

         ];

        $request->validate($rules, $messages);

        $ingrediente = new Ingredientes;
        $ingrediente->fill($request->all());
        $ingrediente->user_id = \Auth::user()->id;
        $ingrediente->save();

      return redirect('/ingredientes');
    }

    public function show($id)
    {
        $ingrediente=Ingredientes::find($id);
        return view('ingredientes.show', ['ingrediente'=>$ingrediente]);
    }

    public function edit($id)
    {
        $ingrediente=Ingredientes::find($id);
        $this->authorize('view', $ingrediente);
        return view('ingredientes.edit', ['ingrediente'=>$ingrediente]);
    }

    public function update(Request $request, $id)
    {
        $rules=[
            'name' => 'required|max:255'  ,
            'user_id' => 'exists:users,id' ,
         ];

         $messages=[
           'required'=>'El nombre debe estar requerido',
           'max'=>'maximo 255 caracteres'

         ];

        $request->validate($rules, $messages);

        $ingrediente = Ingredientes::find($id);
        $ingrediente->fill($request->all());
        $ingrediente->user_id = \Auth::user()->id;
        $ingrediente->save();

      return redirect('/ingredientes');
    }

    public function destroy($id)
    {
         Ingredientes::destroy($id);
        return back();
    }

    public function cook(request $request){

        if(Session::has('receta')){
        $recetas = Session::get('receta');
        $ingredientes = Ingredientes::all();
        $request->session()->put('recetas', $recetas);
        return view('ingredientes.session',[ 'recetas' => $recetas,'ingredientes'=>$ingredientes]);

    } else {
        $modules = Module::all();
        return view('recetas.index', ['recetas' => $recetas]);
    }

    }

    public function añadirIngrediente(Request $request,$id){
        $ingrediente=Ingredientes::find($id);
        $ingredientes=Session::get('ingredientes');

        if(isset($ingredientes[$ingrediente->id])){
         unset($ingredientes[$id]);
     } else {
         $ingredientes[$id]=$ingrediente;

     }

     Session::put('ingredientes',$ingredientes);

     return back();
    }

    public function añadirCantidad(Request $request,$id){
        $ingrediente=Ingredientes::find($id);
        $quantity=$request->input('quantity');
        $ingrediente->recetas()->syncWithoutDetaching( [$ingrediente=>['quantity'=>$quantity]]);
        $rules=[
        'quantity' =>'required|numeric|max:2',
    ];
    $messages = [
        'required' => 'The :attribute field is required.',
        'max' => 'The :attribute numeric is  max :2 ',
        ];
            $request->validate($rules,$messages);
            return back();
    }

}
