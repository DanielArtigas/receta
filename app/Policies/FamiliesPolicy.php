<?php

namespace App\Policies;

use App\User;
use App\Families;
use Illuminate\Auth\Access\HandlesAuthorization;

class FamiliesPolicy 
{
    use HandlesAuthorization;

    public function index(User $user, Families $families)
    {
        //
    }

    public function view(User $user, Families $families)
    {
        return $user->id == 1 || $user->id == $families->user_id;
    }

    public function create(User $user)
    {
        //
    }

    public function update(User $user, Families $families)
    {
        //
    }

    public function delete(User $user, Families $families)
    {
        //
    }

    public function restore(User $user, Families $families)
    {
        //
    }

    public function forceDelete(User $user, Families $families)
    {
        //
    }

    public function manage(User $user)
    {
        return $user->id == 1;
    }
}
