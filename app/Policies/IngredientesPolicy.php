<?php

namespace App\Policies;

use App\User;
use App\Ingredientes;
use Illuminate\Auth\Access\HandlesAuthorization;

class IngredientesPolicy
{
    use HandlesAuthorization;

    public function index(User $user, Ingredientes $ingredientes)
    {
        //
    }

    public function view(User $user, Ingredientes $ingredientes)
    {
        return $user->id == 1 || $user->id == $ingredientes->user_id;
    }

    public function create(User $user)
    {
        //
    }

    public function update(User $user, Ingredientes $ingredientes)
    {
        //
    }

    public function delete(User $user, Ingredientes $ingredientes)
    {
        //
    }

    public function restore(User $user, Ingredientes $ingredientes)
    {
        //
    }

    public function forceDelete(User $user, Ingredientes $ingredientes)
    {
        //
    }
    
    public function manage(User $user) {
        return $user->id == 1;
    }
}
