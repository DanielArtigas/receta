<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recetas extends Model
{
    protected $fillable =['name','time','user_id','family_id'];
    
    public function ingredientes() {
        return $this->belongsToMany(Ingredientes::class)->withPivot('quantity');
    }

    public function familias() {
        return $this->belongsTo(Families::class);
    }

    public function pasos() {
        return $this->hasMany(Pasos::class);
    }

    public function user() {
    return  $this->belongsTo(User::class);
    }
}
