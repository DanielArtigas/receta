<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pasos extends Model
{
    public function recetas() {
        return $this->belongsTo(Recetas::class);
    }
}
