<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Families extends Model
{
    protected $fillable =['name','user_id'];
    
    public function recetas() {
        return $this->hasMany(Recetas::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
