@extends('layouts.app')

@section('title', 'Recetas')

@section('content')
            <h1>
               Receta lista <?php echo $receta->id ?>
            </h1>

            <ul>
                <li>Receta Nombre: {{$receta->name}} </li>
                <li>Tiempo: {{$receta->time}}</li>

            </ul>

            <a href="/recetas" class="btn btn-primary"  role="button">Vuelve a la receta </a>


@endsection