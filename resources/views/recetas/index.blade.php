@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row content">
    <div class="col-sm-12 text-left">
      <h1>Recetas</h1>

        <a  href="/recetas/create" class="btn btn-primary"  role="button" >Crear</a>
        <a  href="/ingredientes/cook" class="btn btn-primary"  role="button" >Session</a>
      <table  class="table table-striped table-hover">
        <thead>
            <tr>
                <th>Nombre</th>
                <th>Tiempo </th>
            </tr>
        </thead>

        <tbody>
            @foreach($recetas as $receta )
            <tr>
               <td>{{$receta->name}}</td>
               <td>{{$receta->time}}</td>

               <td><a  href="/recetas/<?php echo $receta->id ?>" class="btn btn-primary"  role="button" >Ver</a></td>
                <td><a  href="/recetas/<?php echo $receta->id ?>/edit" class="btn btn-primary"  role="button" >Editar</a></td>
                <td><a  href="/recetas/<?php echo $receta->id ?>/nombreReceta" class="btn btn-primary"  role="button" >Receta Guardada</a></td>
               <td>
                  <form method="post" action="/recetas/{{$receta->id}}">
                      {{ csrf_field() }}

                      <input type="hidden" name="_method" value="delete">
                      <input type="submit" value="Borrar" class="btn btn-danger"  role="button">

                  </form>
              </td>


          </tr>

          @endforeach


      </tbody>
  </table>

</div>
</div>
</div>
@endsection