@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <h1>Create de Familias</h1>
            <form method="post" action="/families">
                {{ csrf_field() }}

                <div class="form-group">
                    <label>Nombre</label>
                    <input class="form-control" type="text" name="name" value="{{old('name')}}">

                    @if ($errors->first('name'))
                    <div class="alert alert-danger ">
                        {{$errors->first('name')}}
                    </div>
                    @endif
                </div>

                <input type="submit" value="Crear familia" class="btn btn-primary"  role="button">

                <a href="/families" class="btn btn-primary"  role="button">Volver</a>
            </form>
        </div>

    </div>
</div>
@endsection