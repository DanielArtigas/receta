@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row content">
    <div class="col-sm-12 text-left">
      <h1>Recetas Session</h1>
      @if(Session::has('receta'))

      Nombre de la receta : {{Session::get('receta')->name}}

      @endif
      <br><br>

      <ul>
        @if (Session::has('ingredientes'))
        @foreach(Session::get('ingredientes') as $ingrediente)
        <li> Nombre del ingrediente:{{ $ingrediente->name }} </li>
        @endforeach
        @endif
      </ul>


      <a  href="/recetas/guardarCesta" class="btn btn-primary"  role="button" >Guardar</a>
      <a  href="/recetas/borrar" class="btn btn-danger"   role="button" >Borrar</a>

      <table  class="table table-striped table-hover">
        <thead>
          <tr>
            <th>Ingrediente</th>
            <th>Cantidad</th>
            <th>Guardar</th>

          </tr>
        </thead>

        <tbody>
          @foreach($ingredientes as $ingrediente )

          <tr>
           <td>{{$ingrediente->name}}</td>

           <td></td>

           <td>@if (isset(Session::get('ingredientes')[$ingrediente->id]))
            <a  href="/ingredientes/<?php echo $ingrediente->id ?>/añadirIngrediente" class="btn btn-danger"  role="button" >-</a>
            @endif
         </td>
       </tr>

       @endforeach
     </tbody>
   </table>



   <h2>
    Añade cantidad de ingredientes
  </h2>
  <ul>
    <form class="form"  method="post" action="/ingredientes/{{$ingrediente->id}}/recetas">
    {{ csrf_field() }}

    <div class="form-group">
     <label>Cantidad</label>
     <input class="form-control" type="number" name="quantity" value="{{old('quantity')}}">

     @if ($errors->first('quantity'))
     <div class="alert alert-danger ">
      {{$errors->first('quantity')}}
    </div>
    @endif

  </div>

  <input type="submit" value="Añadir" class="btn btn-primary"  role="button">

</form>

</ul>


<h2>
    Añade los pasos de la receta
  </h2>
  <ul>
    <form class="form"  method="post" action="/ingredientes/{{$ingrediente->id}}/pasos">
    {{ csrf_field() }}

    <div class="form-group">
     <label>Pasos</label>
     <input class="form-control" type="text" name="description" value="{{old('description')}}">

     @if ($errors->first('description'))
     <div class="alert alert-danger ">
      {{$errors->first('description')}}
    </div>
    @endif

  </div>

  <input type="submit" value="Añadir" class="btn btn-primary"  role="button">

</form>

</ul>

</div>
</div>
</div>
@endsection