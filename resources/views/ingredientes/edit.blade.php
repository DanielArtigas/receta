@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <h1>Editar de Ingredientes</h1>
            <form class="form"  method="post" action="/ingredientes/{{$ingrediente->id}}">
                {{ csrf_field() }}

                 <input type="hidden" name="_method" value="put">

                <div class="form-group">
                    <label>Nombre</label>
                    <input class="form-control" type="text" name="name"
                    value="{{$ingrediente->name}}">

                    @if ($errors->first('name'))
                    <div class="alert alert-danger ">
                        {{$errors->first('name')}}
                    </div>
                    @endif


                </div>

                <input type="submit" value="Editar Ingredientes" class="btn btn-primary"  role="button">

                <a href="/ingredientes" class="btn btn-primary"  role="button">Volver</a>
            </form>
        </div>

    </div>
</div>
@endsection