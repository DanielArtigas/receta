@extends('layouts.app')

@section('title', 'Ingredientes')

@section('content')
            <h1>Lista de Ingredientes <?php echo $ingredientes->id ?></h1>

            <ul>
                <li>Nombre: {{$ingredientes->name}} </li>
            </ul>

            <a href="/ingredientes" class="btn btn-primary"  role="button">Volver</a>


@endsection