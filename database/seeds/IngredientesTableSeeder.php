<?php

use Illuminate\Database\Seeder;

class IngredientesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ingredientes')->insert([
            'name' => 'macarrones',
        ]);
        DB::table('ingredientes')->insert([
            'name' => 'ternasco',
        ]);
        DB::table('ingredientes')->insert([
            'name' => 'merluza',
        ]);
        DB::table('ingredientes')->insert([
            'name' => 'acelga',
        ]);
        DB::table('ingredientes')->insert([
            'name' => 'helado',
        ]);
        DB::table('ingredientes')->insert([
            'name' => 'vino',
        ]);
        DB::table('ingredientes')->insert([
            'name' => 'bizcocho',
        ]);
        DB::table('ingredientes')->insert([
            'name' => 'fideos',
        ]);
         DB::table('ingredientes')->insert([
            'name' => 'setas',
        ]);
        DB::table('ingredientes')->insert([
            'name' => 'centollo',
        ]);
         DB::table('ingredientes')->insert([
            'name' => 'espaguetis',
        ]);
        DB::table('ingredientes')->insert([
            'name' => 'panceta',
        ]);
        DB::table('ingredientes')->insert([
            'name' => 'bacalado',
        ]);
        DB::table('ingredientes')->insert([
            'name' => 'borraja',
        ]);
        DB::table('ingredientes')->insert([
            'name' => 'tarta',
        ]);
        DB::table('ingredientes')->insert([
            'name' => 'agua',
        ]);
        DB::table('ingredientes')->insert([
            'name' => 'bollo',
        ]);
        DB::table('ingredientes')->insert([
            'name' => 'ajo',
        ]);
         DB::table('ingredientes')->insert([
            'name' => 'jamon',
        ]);
        DB::table('ingredientes')->insert([
            'name' => 'gamba',
        ]);
    }
}
