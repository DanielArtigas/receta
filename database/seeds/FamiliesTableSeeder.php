<?php

use Illuminate\Database\Seeder;

class FamiliesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('families')->insert([
            'name' => 'Pasta',
        ]);
        DB::table('families')->insert([
            'name' => 'Carnes',
        ]);
        DB::table('families')->insert([
            'name' => 'Pescados',
        ]);
        DB::table('families')->insert([
            'name' => 'Verduras',
        ]);
        DB::table('families')->insert([
            'name' => 'Postres',
        ]);
        DB::table('families')->insert([
            'name' => 'Bebidas',
        ]);
        DB::table('families')->insert([
            'name' => 'Reposteria',
        ]);
        DB::table('families')->insert([
            'name' => 'Sopas',
        ]);
         DB::table('families')->insert([
            'name' => 'Pizzas',
        ]);
        DB::table('families')->insert([
            'name' => 'Mariscos',
        ]);
    }
}
