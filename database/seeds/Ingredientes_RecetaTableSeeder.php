<?php

use Illuminate\Database\Seeder;

class Ingredientes_RecetaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $quantity=1;
        $ingrediente=\App\Ingredientes::find(1);
        $ingrediente->recetas()->attach(1, ['quantity' => $quantity]);

        $ingrediente=\App\Ingredientes::find(1);
        $ingrediente->recetas()->attach(2, ['quantity' => $quantity]);

        $ingrediente=\App\Ingredientes::find(1);
        $ingrediente->recetas()->attach(3, ['quantity' => $quantity]);
    }
}
