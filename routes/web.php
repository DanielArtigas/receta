<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('/familias', 'FamiliesController');
Route::resource('/recetas', 'RecetasController');
Route::get('/ingredientes/cook', 'IngredientesController@cook');
Route::get('/recetas/{id}/nombreReceta', 'RecetasController@nombreReceta');
Route::post('/ingredientes/{id}/recetas', 'IngredientesController@añadirCantidad');
Route::get('/ingredientes/{id}/añadirIngrediente','IngredientesController@añadirIngrediente');
Route::resource('/ingredientes', 'IngredientesController');

Route::get('/recetas/{id}/deshacerCantidad', 'RecetasController@deshacerCantidad');
Route::post('/recetas/{id}/añadirPasos', 'RecetasController@añadirPasos');

Route::get('/recetas/guardarCesta', 'RecetasController@guardarCesta');
Route::get('/recetas/borrar', 'RecetasController@borrar');

